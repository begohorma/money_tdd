//
//  WadTest.swift
//  Money
//
//  Created by Begoña Hormaechea on 25/4/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import XCTest

@testable import Money

class WadTest: XCTestCase {
    
    let emptyWad : Wad = Wad()
    let singleBillWad = Wad(amount: 42, currency: "$")
    let  tenEuros =  Wad (amount:  10 , currency: "€" )
    let  tenDollars =  Wad (amount:  10 , currency:  "$" )
    let twentyEuro = Wad(amount: 20, currency: "€")
     let fiftyEuro = Wad(amount: 50, currency: "€")
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    func testCanCreateWad(){
        
        XCTAssertNotNil(Wad())
        
    }
    
    func testStringConversion(){
        XCTAssertEqual("\(emptyWad)", "Empty")
        XCTAssertEqual("\(tenEuros)", "€ 10")
    }
    
    func testStringConversionDollar(){
        XCTAssertEqual("\(singleBillWad)", "$ 42")
        XCTAssertEqual("\(tenDollars)", "$ 10")
    }
    
    func testEquality(){
        
        // identity
        XCTAssertEqual(emptyWad, emptyWad)
        XCTAssertEqual(singleBillWad, singleBillWad)
        
        XCTAssertNotEqual(emptyWad, singleBillWad)
        
        // equivalence
       
        
        let fifty2 = Wad(amount: 10, currency: "€")
            .plus(tenEuros).plus(tenDollars).plus(tenDollars).plus(tenEuros)
        let fifty3 = Wad(amount: 30, currency: "€").plus(tenDollars).plus(tenEuros)
        
        XCTAssertEqual(fiftyEuro, fifty2)
        XCTAssertEqual(fiftyEuro, fifty3)
        XCTAssertEqual(fifty2, fifty3)
        
    }
    func testWadAddition(){
        let twentyWad = Wad(amount: 10, currency: "€").plus(tenDollars).plus(Wad(amount: 30, currency: "$"))
        
        XCTAssertEqual(twentyWad, fiftyEuro)
    }
    func  testSimpleAddition(){
        XCTAssertEqual(singleBillWad.plus(Wad (amount:8,currency:  "$" )),  Wad(amount:50,currency:"$"))
    }
    func  testSimpleMultiplication(){
        let  eightyFour =  singleBillWad.times(2)
         XCTAssertEqual (eightyFour,  Wad (amount: 84 , currency:  "$" ))
    }

}
