//
//  BillTest.swift
//  Money
//
//  Created by Begoña Hormaechea on 19/4/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import XCTest

@testable import Money

class EuroTest: XCTestCase {
    
    let five = Bill(amount: 5)
    let otherFive = Bill(amount: 5)
    let ten = Bill(amount: 10)
    var broker : Broker!
    let twentyDollar = Bill(amount: 20, currency: "$")
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        broker = Broker()
    
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testCanCreateBill(){
        
        XCTAssertNotNil(Bill())
        
    }
    
    func testSimpleMultiplication(){
        let twentyFour = Bill(amount:12).times(2)
        XCTAssertEqual(twentyFour._amount, 24)
    }
    
    func testEquality(){
        
        XCTAssertEqual(otherFive, otherFive)
        XCTAssertEqual(five, otherFive)
        
        XCTAssertNotEqual(five, ten)
        
    }
    func testThatObjectWithEqualHashAreEqual(){
        
        XCTAssertEqual(five.hashValue, otherFive.hashValue)
        XCTAssertNotEqual(ten.hashValue, otherFive.hashValue)
    }

    func testStringConversion(){
        XCTAssertEqual("\(five)", "€ 5")
    }
    
    func testStringConversionDollar(){
        XCTAssertEqual("\(twentyDollar)", "$ 20")
    }
    func testSimpleAddition(){
        
        XCTAssertEqual(ten, five.plus(otherFive))
    }

    func testSimpleReduction(){
        
        XCTAssertEqual(try five.reduced(to: "€", broker: broker), five)
//        XCTAssertEqual(five.reduced(to: "€", broker: broker), five)
    }
    
    func testReduction(){
        broker.addRate(currency: "€", rateToConvertToEuro: 1)
        broker.addRate(currency: "$", rateToConvertToEuro: 2)
        
       XCTAssertEqual(try ten.reduced(to: "$", broker: broker), twentyDollar)
//        XCTAssertEqual(try twentyDollar.reduced(to: "€", broker: broker), ten)
    }

//    func testReductionDollarToEuro(){
//        //No funciona si se trabaja con int
//        broker.addRate(currency: "€", rateToConvertToEuro: 1)
//        broker.addRate(currency: "$", rateToConvertToEuro: 2)
//        
//        XCTAssertEqual(try ten.reduced(to: "$", broker: broker), twentyDollar)
//        XCTAssertEqual(try twentyDollar.reduced(to: "€", broker: broker), ten)
//    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
