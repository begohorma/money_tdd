//
//  Money.swift
//  Money
//
//  Created by Begoña Hormaechea on 24/4/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation

protocol Money{

    init(amount: Int, currency:Currency)
    
    init()
    
    func times( _ n:Int)-> Self
    
    func plus( _ e: Self)-> Self
    
//    func reduced(to: Currency, broker: Rater) throws -> Self
    //la conversión devuelve Bill
    
    func reduced(to: Currency, broker: Rater) throws -> Bill
}


