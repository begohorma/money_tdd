//
// Created by Begoña Hormaechea on 20/4/17.
// Copyright (c) 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation

typealias Rate = Int
//diccionario de cada divisia con su tasa de cambio.
typealias RatesDictionary = [String:Rate]

enum BrokerError : Error{
    case currencyRateNotFound(Currency)

}

//Protocols
//prototcolo que deben convertir todo los brokers que devuelven un ratio de cambio
protocol Rater{
    func rate(from:Currency,to:Currency) throws ->Rate
}

//Default implementations
extension Rater{
    //Implementación por defecto de Rater que devuelve siempre un ratio de 1
    func rate(from:Currency,to:Currency) throws ->Rate{
        return 1
    }
}

//Broker que implementa Rater con su implementación por defecto
struct UnityBroker:Rater{}


//Broker implementa el protocolo Rater
struct Broker:Rater{

    var _rates = RatesDictionary()
    
    mutating func addRate(currency:Currency,rateToConvertToEuro: Int){
        _rates[currency] = rateToConvertToEuro
    }
    
    func rate(from: Currency, to: Currency) throws -> Rate{
        if from == to {
            return 1
        }else{
            guard let fromRate = _rates[from] else{
                throw BrokerError.currencyRateNotFound(from)
            }
            
            guard let toRate = _rates[to] else{
                throw BrokerError.currencyRateNotFound(to)
            }
            
            if fromRate == 1 {
                return toRate
            }else{
                return toRate/fromRate
            }
            
            
        }
        
    }
    
    
}
