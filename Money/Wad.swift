//
//  Wad.swift
//  Money
//
//  Created by Begoña Hormaechea on 24/4/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation

typealias Bills = [Bill]

struct Wad {
    //El fajo es un array de billetes
    var _bills = Bills()
    
    var billCount : Int{
        get{
            return _bills.count
        }
    }
    
}

extension Wad : Money{
    init(amount: Int, currency:Currency){
        //Al crear un fajo con una cantidad y divisa se crea un billete con la cantidad y divisa indicadas
        _bills.append(Bill(amount: amount, currency: currency))
    }

    
    
//    func times( _ n:Int)-> Wad {
//        // a cada billete del wad se le llama su método times pasandole n
//        // se guarda en un nuevo array
//        var multi = Bills()
//        
//        for b in _bills{
//         multi.append(b.times(n))
//        }
//        
//        return Wad(_bills: multi)
//        }
    
    func times( _ n:Int)-> Wad {
       
        let multi = _bills.map({$0.times(n)})
        
        return Wad(_bills: multi)
    }
        
    
    
    func plus( _ e: Wad)-> Wad {
       return Wad(_bills: _bills + e._bills)
    }
    
    func plus(_ b:Bill) -> Wad{
        //añadir un billete al array de billetes.
        //Se usa el constructor por defecto del struct para poder crear un wad usando un array
        var bills = _bills
        bills.append(b)
        return Wad(_bills: bills)

    }
    
//    func reduced(to: Currency, broker: Rater) throws -> Bill{
//        // recorrer los billetes del fajo y convertir su valor a la divisa indicada
//        
//        //inicializar la cantidad como un billete de 0 de la divisa indicada
//        var wadAmount = Bill(amount: 0, currency: to)
//        do
//         {
//            for b in _bills{
//                wadAmount = try wadAmount.reduced(to: to, broker: broker).plus(try b.reduced(to: to, broker: broker))
//            }
//            
//        }catch{
//            print(error)
//        }
//        
//        return wadAmount
//    }
    
    func reduced(to: Currency, broker: Rater) throws -> Bill{
       
         let  wadAmount = _bills.reduce(Bill(amount: 0, currency: to), {try! $0.reduced(to: to, broker: broker).plus( try! $1.reduced(to: to, broker: broker))})

        return wadAmount
    }


}

// MARK: -  Extensions

extension Wad: Equatable{
    public static func ==(lhs: Wad, rhs: Wad) -> Bool{
        
    //convertir ambos a euros para comparar
    //Utilizar el unitybroker para simplificar
        let broker = UnityBroker()
        
//        return lhs._amount == rhs._amount
            let lw = try! lhs.reduced(to: "€", broker: broker)
            let rw = try! rhs.reduced(to: "€", broker: broker)
           
            return lw == rw
    }
    
}


extension Wad: CustomStringConvertible{
    public var description: String {
        get{
            if billCount == 0{
                return "Empty"
            }else if billCount == 1 {
                return (_bills.first?.description)!
            }else{
                var descri = ""
                for b in _bills{
                    descri = descri + " + " + b.description
                }
                return descri
            }
        }
    }
}


