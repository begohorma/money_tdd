//
//  Bill.swift
//  Money
//
//  Created by Begoña Hormaechea on 19/4/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation

typealias Currency = String

struct Bill:Money {
    let _amount: Int
    let _currency : Currency
    
    init(amount: Int, currency:Currency = "€") {
        _amount = amount
        _currency = currency
    }
    
    init(){
        _amount = 0
        _currency = "€"
    }
    
    
    func times( _ n:Int)-> Bill {
        return Bill(amount: _amount * n)
    }
    
    func plus( _ e: Bill)-> Bill {
        return Bill(amount: _amount + e._amount)
    }

    func reduced(to: Currency, broker: Rater) throws -> Bill {
        //obtener tasa de cambio de divisa from(la del objeto money) a divisa to
        do{
            let rate = try broker.rate(from: _currency, to: to)
            //crear nuevo objeto money aplicandole el ratio de cambio a la cantidad y asignandole la currency destino
            return Bill(amount: _amount * rate, currency: to)
            
        }catch{
            print(error)
        return self // ????
        }
    }
    
    
}

// MARK: -  Extensions

extension Bill: Equatable{
    public static func ==(lhs: Bill, rhs: Bill) -> Bool{
        return lhs._amount == rhs._amount
    }
    
}

extension Bill: Hashable{
    public var hashValue: Int{
        get{
            return _amount.hashValue
           }
    }
}

extension Bill: CustomStringConvertible{
    public var description: String {
        get{
            return "\(_currency) \(_amount)"
        }
    }
}

